import src.avion.domain.Avion;
import src.avion.domain.AvionRepository;
import src.avion.domain.Passager;
import src.avion.infra.AvionRepositoryInMemory;

public class Main{
    public static void main(String[] args) {
        Avion avion = new Avion(1, 20);
        System.out.println(avion.getId());
        avion.reserverPlace(new Passager("x","b"));
        AvionRepository repo = new AvionRepositoryInMemory();
        repo.save(avion);
        Avion avionSaved = repo.findAvionById(avion.getId());
        System.out.println(avionSaved.getId());
    }
} 

