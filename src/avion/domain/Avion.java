package src.avion.domain;

import java.util.ArrayList;

public class Avion {
    private ArrayList<Place> places;
    private Integer id;
    private Integer nbPlaces;

    public Avion(Integer id, Integer nbPlaces){
        this.id = id;
        this.nbPlaces = nbPlaces;
        this.places = new ArrayList<Place>(nbPlaces);
    } 

    public void reserverPlace(Passager passager){
        if(this.getNbPlacesLibres()>0){
            if(!this.places.contains(passager)){
                Integer id = this.choosePlace();
                this.places.add(new Place(id));
                this.places.get(id).reserver(passager);
            } 
        } 
        
    } 

    public Integer getId(){
        return this.id;
    } 

    private Integer getNbPlacesLibres(){
        return this.nbPlaces - this.places.size();
    } 

    private Integer choosePlace(){
        return this.places.size();
    } 
}
