package src.avion.domain;

public interface AvionRepository {
    void save(Avion avion);
    Avion findAvionById(Integer id);
}
