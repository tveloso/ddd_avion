package src.avion.domain;

public class Place {
    private Integer id;
    private boolean libre;
    private Passager passager;

    public Place(Integer id){
        this.id = id;
        this.libre = true;
    }
    
    public boolean estLibre(){
        return this.libre;
    } 

    public void reserver(Passager passager){
        if(this.estLibre()){
            this.passager = passager;
            this.libre = false;
        }  
    } 

    public Passager getPassager(){
        return this.passager;
    } 
}
