package src.avion.infra;

import java.util.Map;
import java.util.HashMap;
import src.avion.domain.AvionRepository;
import src.avion.domain.Avion;

public class AvionRepositoryInMemory implements AvionRepository{
    private Map<Integer,Avion> avions;

    public AvionRepositoryInMemory(){
        this.avions = new HashMap<Integer,Avion>();
    } 
    
    public void save(Avion avion){
        this.avions.put(avion.getId(),avion);
    }
    
    public Avion findAvionById(Integer id){
        return this.avions.get(id);
    } 
}
